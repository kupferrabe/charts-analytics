import Api from '@/services/Api'

export default {
  fetchPosts () {
    return Api().get('posts')
  },

  addPost (params) {
    return Api().post('posts', params)
  },

  addFile (params) {
    return Api().post('upload', params)
      .then(function () {
        console.log('SUCCESS!!')
      })
      .catch(function () {
        console.log('FAILURE!!')
      })
  },

  updatePost (params) {
    return Api().put('posts/' + params.id, params)
  },

  getPost (params) {
    return Api().get('post/' + params.id)
  },

  deletePost (id) {
    return Api().delete('posts/' + id)
  }
}
