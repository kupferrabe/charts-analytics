var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var SequenceSchema = new Schema({
  name: String,
  symbol: String,
  exchange: String,
  start: Number,
  stop: Number,
  length: Number
});

var Sequence = mongoose.model("Sequence", SequenceSchema);
module.exports = Sequence;
