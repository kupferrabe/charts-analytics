var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var TickSchema = new Schema({
  sequenceId: Number,
  last: Number,
  timestamp: Number
});

var Tick = mongoose.model("Tick", TickSchema);
module.exports = Tick;

/*
export interface Ticker {
  ask: number;
  average?: number;
  baseVolume?: number;
  bid: number;
  change?: number;
  close?: number;
  datetime: string;
  first?: number;
  high: number;
  info: object;
  last?: number;
  low: number;
  open?: number;
  percentage?: number;
  quoteVolume?: number;
  symbol: string,
  timestamp: number;
  vwap?: number;
}

symbol: ticker['symbol'],
                                        price: ticker['last'].toFixed (8),
                                        datetime: ticker['datetime'],
*/
