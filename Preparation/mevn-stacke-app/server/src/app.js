const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const morgan = require('morgan')

const http = require('http')
const formidable = require('formidable')
const fs = require('fs')
const path = require('path')

const app = express()

app.use(morgan('combined'))
app.use(bodyParser.json())
app.use(cors())

app.listen(process.env.PORT || 8081)

  //-------------------------------------------------------------

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/posts');
var db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", function(callback){
  console.log("Connection Succeeded");
});

var Post = require("../models/post");

// Add new post
app.post('/posts', (req, res) => {
  var db = req.db;
  var title = req.body.title;
  var description = req.body.description;
  var new_post = new Post({
    title: title,
    description: description
  })

  new_post.save(function (error) {
    if (error) {
      console.log(error)
    }
    res.send({
      success: true,
      message: 'Post saved successfully!'
    })
  })
})

// Fetch all posts
app.get('/posts', (req, res) => {
  Post.find({}, 'title description', function (error, posts) {
    if (error) { console.error(error); }
    res.send({
      posts: posts
    })
  }).sort({_id:-1})
})

// Fetch single post
app.get('/post/:id', (req, res) => {
  var db = req.db;
  Post.findById(req.params.id, 'title description', function (error, post) {
    if (error) { console.error(error); }
    res.send(post)
  })
})

// Update a post
app.put('/posts/:id', (req, res) => {
  var db = req.db;
  Post.findById(req.params.id, 'title description', function (error, post) {
    if (error) { console.error(error); }

    post.title = req.body.title
    post.description = req.body.description
    post.save(function (error) {
      if (error) {
        console.log(error)
      }
      res.send({
        success: true
      })
    })
  })
})

// Delete a post
app.delete('/posts/:id', (req, res) => {
  var db = req.db;
  Post.remove({
    _id: req.params.id
  }, function(err, post){
    if (err)
      res.send(err)
    res.send({
      success: true
    })
  })
})

// Upload Endpoint
app.post('/upload', (req, res) => {
  var form = new formidable.IncomingForm();
  form.parse(req, function(err, fields, files) {
    const keys = Object.keys(files)
    for(let k of keys) {
      console.log('Key:'+k);
      let oldPath = files[k].path
          fileSize = files[k].size,
          fileExt = files[k].name.split('.').pop(),
          index = indexAfterLastPathSeparator(oldPath),
          fileName = oldPath.substr(index),
          newPath = path.join(process.env.PWD, '/uploads/', fileName + '.' + fileExt);
          console.log('====>');
          console.log('oldPath: '+oldPath);
          console.log('newPath: '+newPath);

          moveFile(oldPath, newPath, res);
    }
  })
})

function indexAfterLastPathSeparator(path){
  index = path.lastIndexOf('/');
  index = path.lastIndexOf('/');
  if(index == -1){
    index = path.lastIndexOf('\\');
  }

  return  index + 1
}

function moveFile(oldPath, newPath, res){
  fs.readFile(oldPath , function(err, data) {
    console.log(err);
    fs.writeFile(newPath, data, function(err) {
      console.log(err);
      fs.unlink(oldPath, function(){
/*
        if (err) {
          res.status(500);
          res.json({'success': false});
        } else {
          res.status(200);
          res.json({'success': true});
        }
*/
        if(err) throw err;
//        res.send("File uploaded to: " + newPath);
      });
    }); 
  }); 
}